package com.wordpress.allrightsunreserved.SudokuSolver;

import java.io.*;
import com.wordpress.allrightsunreserved.SudokuSolver.SudokuConstants;

/**
 * This class reads the passed in text file for the sudoku puzzle, kicks off the solving, and outputs
 * the solution to standard output
 * @author fsheikh
 *
 */
public class MainClass {
	
	public static void main(String[] args) {
		if (args.length == 0)
		{
			System.out.println("A text file representing the sudoku puzzle must be provided");
			return;
		}
		
		String filePath = args[0];

		int[][] grid = readSudokuFromFile(filePath);	
		
		SudokuEngine sudokuEngine = new SudokuEngine(grid);
		
		sudokuEngine.run();
	}
	
	/**
	 * Reads the sudoku puzzle contained in the file given by the passed in path
	 * and returns a 2D int matrix representation.
	 * @param filePath
	 * @return
	 */
	private static int[][] readSudokuFromFile(String filePath)
	{
		int[][] sudokuGrid = new int[9][9];
		
		try {
	    	BufferedReader br = new BufferedReader(new FileReader(filePath));
	        String line = br.readLine(); 
	       
	        int row = SudokuConstants.TOP_ROW; //Start at the top row
	      
	        while (line != null && row >= 0)
	        {
	        	int beginIndex = 0;
	        	for (int col = 0; col <= SudokuConstants.LAST_COL; col++, beginIndex+=2) 
	        	{
	        		String currentVal = line.substring(beginIndex, beginIndex+1);
		        	
		        	if (currentVal.equals(" "))
		        	{
		        		sudokuGrid[row][col] = SudokuConstants.UNASSIGNED;
		        	}
		        	else
		        	{
		        		sudokuGrid[row][col] = Integer.parseInt(currentVal);
		        	}	
	        	}
	        
	        	row--;
	        	line = br.readLine();
	        }
	   
	        br.close();
	    } catch (Exception e)
	    {
	    	System.out.println("There was an error reading the Sudoku puzzle from the given file.");
	    }
		
		return sudokuGrid;
	}

}
