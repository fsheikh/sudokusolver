package com.wordpress.allrightsunreserved.SudokuSolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.wordpress.allrightsunreserved.SudokuSolver.SudokuConstants;

/**
 * This class represents a Sudoku cell of a Sudoku puzzle
 * @author fsheikh
 *
 */
public class SudokuCell {

	private boolean isChangeable = false;
	private int value;
	
	List<Integer> possibilities = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9));
	
	
	SudokuCell(int val)
	{
		if (val == SudokuConstants.UNASSIGNED)
		{
			this.setIsChangeable(true);
		}
		else
		{
			this.possibilities.clear(); //If this cell was already assigned then we don't have any possibilities
		}
		
		this.setValue(val);
	}

	public boolean isChangeable() {
		return isChangeable;
	}

	private void setIsChangeable(boolean isChangeable) {
		this.isChangeable = isChangeable;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	
}
