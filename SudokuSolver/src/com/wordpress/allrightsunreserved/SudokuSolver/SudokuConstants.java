package com.wordpress.allrightsunreserved.SudokuSolver;

/**
 * This is a public interface for me to place all my Sudoku specific constants
 * @author fsheikh
 *
 */
public interface SudokuConstants {

	public static final int TOP_ROW = 8;
	public static final int LAST_COL = 8;
	public static final int UNASSIGNED = -1;
	
}
