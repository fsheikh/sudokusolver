package com.wordpress.allrightsunreserved.SudokuSolver;

/**
 * This class is a standard class to encapsulate the x and y coordinates to 
 * make the idea of a "location" on a 2D map work
 * @author fsheikh
 *
 */
public class Location {

	private int row;
	private int col;
	
	Location(int row, int col)
	{
		this.setRow(row);
		this.setCol(col);
	}

	public int getCol() {
		return col;
	}

	private void setCol(int col) {
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	private void setRow(int row) {
		this.row = row;
	}
	
	
}
