package com.wordpress.allrightsunreserved.SudokuSolver;

import java.util.ArrayList;

import com.wordpress.allrightsunreserved.SudokuSolver.SudokuConstants;

/**
 * The SudokuEngine is a Sudoku solving class that will take a puzzle and solve it for us
 * @author fsheikh
 *
 */
public class SudokuEngine implements Runnable {
	
	private SudokuCell[][] puzzle = new SudokuCell[9][9];
	private ArrayList<Location> unassignedLocations;
	
	/**
	 * Initialize the SudokuCell based puzzle representation
	 * @param puzzle
	 */
	SudokuEngine(int[][] puzzle)
	{
		for (int row = 0; row <= SudokuConstants.TOP_ROW; row++)
		{
			for (int col = 0; col <= SudokuConstants.LAST_COL; col++)
			{
				this.puzzle[row][col] = new SudokuCell(puzzle[row][col]);
				
				if (this.puzzle[row][col].isChangeable())
				{
					if (unassignedLocations == null)
					{
						unassignedLocations = new ArrayList<Location>();
					}
					unassignedLocations.add(new Location(row, col));
				}
			}
		}
	}
	
	/**
	 * The default run method needed to implement runnable
	 */
	public void run()
	{
		solveSudoku();
	}
	
	/**
	 * This method does some useful preprocessing such as keeping track of all initially
	 * unassigned cells as well as populating the list of possibilities for them.
	 * Then we formally kick off the sudoku solving using a recursive backtracking algorithm
	 */
	private void solveSudoku()
	{
		for (Location unassignedLocation: unassignedLocations)
		{
			populatePossibilities(unassignedLocation);
		}
		
		Boolean Solved = solveSudokuRecursiveBackTracking();
		
		if (Solved)
		{
			printPuzzleSolution();
		}
		else
		{
			System.out.println("No Solution");
		}
	}
	
	/**
	 * Prints the solved puzzle to standard output
	 */
	private void printPuzzleSolution() {
		for (int row = SudokuConstants.TOP_ROW;  row >= 0; row--)
		{
			for (int col = 0; col <=  SudokuConstants.LAST_COL; col++)
			{
				StringBuffer strBuff = new StringBuffer();
				strBuff.append(puzzle[row][col].getValue()).append(" ");
				System.out.print(strBuff.toString());
			}
			System.out.println("");
		}
		
	}

	/**
	 * Solves the puzzle using recursive backtracking
	 * @return true if there is a solution and false if none was found
	 */
	private Boolean solveSudokuRecursiveBackTracking()
	{
		if (unassignedLocations.isEmpty())
			return true;
		
		Location currUnassignedLocation = unassignedLocations.get(0);
		for (int num :  puzzle[currUnassignedLocation.getRow()][currUnassignedLocation.getCol()].possibilities)
		{
			//Try out the possibility
			puzzle[currUnassignedLocation.getRow()][currUnassignedLocation.getCol()].setValue(num);
			unassignedLocations.remove(currUnassignedLocation);
			
			if (isNewAssignmentValid(currUnassignedLocation))
			{
				if (solveSudokuRecursiveBackTracking()) return true;
			}
			puzzle[currUnassignedLocation.getRow()][currUnassignedLocation.getCol()].setValue(SudokuConstants.UNASSIGNED);
			
			unassignedLocations.add(currUnassignedLocation);
		}
		
		return false;
	}
	
	/**
	 * Check to see if the newly assigned value complies with the rules of Sudoku
	 * @param unassignedLocation
	 * @return
	 */
	private boolean isNewAssignmentValid(Location unassignedLocation) {
		SudokuCell unassignedCell = puzzle[unassignedLocation.getRow()][unassignedLocation.getCol()];
		
		//Check col
		for (int row = 0; row <= SudokuConstants.TOP_ROW; row++)
		{
			if (row == unassignedLocation.getRow())
				continue;
			
			int val = puzzle[row][unassignedLocation.getCol()].getValue();
			
			if (val  == unassignedCell.getValue())
			{
				return false;
			}	
		}
		
		//Check row
		for (int col = 0; col <= SudokuConstants.LAST_COL; col++)
		{
			if (col == unassignedLocation.getCol())
				continue;
			
			int val = puzzle[unassignedLocation.getRow()][col].getValue();
			
			if (val == unassignedCell.getValue())
			{
				return false;
			}	
		}
		
		
		//Check sub matrix
		int submatrixRowBegin = (unassignedLocation.getRow()/3)*3;
		int submatrixRowEnd = submatrixRowBegin + 2;
		int submatrixColBegin = (unassignedLocation.getCol()/3)*3;
		int submatrixColEnd = submatrixColBegin + 2;
		
		for (int row = submatrixRowBegin; row <= submatrixRowEnd; row++)
		{
			for (int col = submatrixColBegin; col <= submatrixColEnd; col++)
			{
				if (unassignedLocation.getRow() == row && unassignedLocation.getCol() == col)
					continue;
				
				int val = puzzle[row][col].getValue();
				
				if (val == unassignedCell.getValue())
				{
					return false;
				}
			}
		}
		
		return true;
	}

	/**
	 * This method will check the unassigned location's row, col and submatrix for any numbers already used.
	 * It will remove all already used numbers from the list of possibilities of the SudokuCell at location
	 * @param unassignedLocation
	 */
	private void populatePossibilities(Location unassignedLocation)
	{
		SudokuCell unassignedCell = puzzle[unassignedLocation.getRow()][unassignedLocation.getCol()];
		
		//Check col
		for (int row = 0; row <= SudokuConstants.TOP_ROW; row++)
		{
			int val = puzzle[row][unassignedLocation.getCol()].getValue();
			
			if (val != SudokuConstants.UNASSIGNED)
			{
				unassignedCell.possibilities.remove(Integer.valueOf(val));
			}	
		}
		
		//Check row
		for (int col = 0; col <= SudokuConstants.LAST_COL; col++)
		{
			int val = puzzle[unassignedLocation.getRow()][col].getValue();
			
			if (val != SudokuConstants.UNASSIGNED)
			{
				unassignedCell.possibilities.remove(Integer.valueOf(val));
			}	
		}
		
		
		//Check sub matrix
		int submatrixRowBegin = (unassignedLocation.getRow()/3) * 3;
		int submatrixRowEnd = submatrixRowBegin + 2;
		int submatrixColBegin = (unassignedLocation.getCol()/3) * 3;
		int submatrixColEnd = submatrixColBegin + 2;
		
		for (int row = submatrixRowBegin; row <= submatrixRowEnd; row++)
		{
			for (int col = submatrixColBegin; col <= submatrixColEnd; col++)
			{
				int val = puzzle[row][col].getValue();
				
				if (val != SudokuConstants.UNASSIGNED)
				{
					unassignedCell.possibilities.remove(Integer.valueOf(val));
				}
			}
		}
	}
	

}
